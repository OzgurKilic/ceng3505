package tr.edu.mu.ceng.gui.fragmentexample;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class DynamicFragmentActivity extends Activity implements ExampleFragment.OnExamleFragmentListener,
NextFragment.OnFragmentInteractionListener{

    Button btnNext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dynamic_fragment);

        FragmentTransaction fts = getFragmentManager().beginTransaction();
        ExampleFragment ef = ExampleFragment.newInstance("hello","fragment");
        fts.add(R.id.container,ef);

        fts.commit();
        btnNext = (Button)findViewById(R.id.btnNext);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction fts = getFragmentManager().beginTransaction();
                NextFragment ef = NextFragment.newInstance("hello","fragment");
                fts.replace(R.id.container,ef);
                fts.addToBackStack("examplefragment");
                fts.commit();

            }
        });
    }

    @Override
    public void onButtonClick(String txt) {
        Toast.makeText(this,txt,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTextViewClick() {
        Toast.makeText(this,"Text view Click",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
