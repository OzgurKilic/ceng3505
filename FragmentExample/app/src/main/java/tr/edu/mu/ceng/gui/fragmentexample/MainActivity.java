package tr.edu.mu.ceng.gui.fragmentexample;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {

    Button btnStaticFragment;
    Button btnDynamicFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnStaticFragment = (Button)findViewById(R.id.btnStaticFragment);

        btnStaticFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent  intent = new Intent(MainActivity.this, StaticFragmentActivity.class);
                startActivity(intent);
            }
        });

        btnDynamicFragment = (Button)findViewById(R.id.btnDynamicFragment);

        btnDynamicFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent  intent = new Intent(MainActivity.this, DynamicFragmentActivity.class);
                startActivity(intent);
            }
        });

    }
}
