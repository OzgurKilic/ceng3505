package tr.edu.mu.ceng.gui.fragmentexample;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;

public class StaticFragmentActivity extends Activity implements ExampleFragment.OnExamleFragmentListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_static_fragment);
        ExampleFragment exampleFragment = (ExampleFragment)getFragmentManager().findFragmentById(R.id.example2);

        exampleFragment.button.setText("New Label");
    }

    @Override
    public void onButtonClick(String txt) {
        Toast.makeText(this,txt,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTextViewClick() {
        Toast.makeText(this,"Text view Click",Toast.LENGTH_SHORT).show();
    }
}
