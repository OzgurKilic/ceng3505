package tr.edu.mu.ceng.gui.tictactoe.model;

/**
 * Created by ozgur on 10/26/2017.
 */

class ViewMockup implements BoardListener {

    int state;

    @Override
    public int getState() {
        return state;
    }

    @Override
    public void setState(int i) {
        state = i;
    }
}
