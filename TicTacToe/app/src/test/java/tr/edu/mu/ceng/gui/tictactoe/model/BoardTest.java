package tr.edu.mu.ceng.gui.tictactoe.model;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/**
 * Created by ozgur on 10/26/2017.
 */

public class BoardTest {

    @Test
    public void testMove()throws InvalidMoveException,CellIsNotEmptyException {
        Board board = new Board(3, new ViewMockup());

        board.move(0,0);

        assertEquals(Board.PLAYER_1, board.getMoveAt(0,0));

        board.move(0,2);

        assertEquals(Board.PLAYER_2, board.getMoveAt(0,2));



    }

    @Test (expected = InvalidMoveException.class)
    public void testInvalidMove() throws InvalidMoveException,CellIsNotEmptyException {
        Board board = new Board(3, new ViewMockup());
        board.move(0,3);
        assertEquals(Board.PLAYER_1, board.getMoveAt(0,0));
    }


    @Test (expected = CellIsNotEmptyException.class)
    public void testOccupiedCell() throws InvalidMoveException,CellIsNotEmptyException {
        Board board = new Board(3, new ViewMockup());
        board.move(0,0);
        assertEquals(Board.PLAYER_1, board.getMoveAt(0,0));

        board.move(0,0);
    }


    @Test
    public void testWin() throws InvalidMoveException,CellIsNotEmptyException {
        BoardListener view = new ViewMockup();

        Board board = new Board(3, view);
        board.move(0,0);
        board.move(0,1);
        board.move(1,0);
        board.move(1,1);
        board.move(2,0);

        int state = view.getState();

        assertEquals(BoardListener.PLAYER_1_WIN, state);
    }

}
