package tr.edu.mu.ceng.gui.tictactoe.model;

/**
 * Created by ozgur on 10/26/2017.
 */

public interface BoardListener {
    int PLAYER_1_WIN = 1;
    int PLAYER_2_WIN = 2;
    int getState();

    void setState(int i);
}
