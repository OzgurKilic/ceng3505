package tr.edu.mu.ceng.gui.tictactoe;

import android.os.Bundle;
import tr.edu.mu.ceng.gui.tictactoe.model.Board;
import tr.edu.mu.ceng.gui.tictactoe.model.BoardListener;
import tr.edu.mu.ceng.gui.tictactoe.model.CellIsNotEmptyException;
import tr.edu.mu.ceng.gui.tictactoe.model.InvalidMoveException;

/**
 * Created by ozgur on 10/26/2017.
 */

public class BoardPresenter implements BoardListener {
    BoardView view;
    Board model;
    int state;

    public BoardPresenter(BoardView view) {
        this.view = view;
        model = new Board(3,this);

    }

    public void restoreBoard(Bundle savedInstanceState) {
        model = (Board) savedInstanceState.getSerializable("model");
    }

    @Override
    public int getState() {
        return state;
    }

    @Override
    public void setState(int i) {
        state = i;
        if (state == PLAYER_1_WIN) {
            view.showMessage("X wins");
        }else if (state == PLAYER_2_WIN){
            view.showMessage("O wins");
        }
    }

    public int getMoveAt(int row, int col) {
        return model.getMoveAt(row, col);
    }

    public void save(Bundle outState) {
        outState.putSerializable("model", model);
    }

    public void move(int row, int col) {
        try {
            model.move(row, col);
            if (model.getMoveAt(row,col) == Board.PLAYER_1){
                view.updateLabel(row,col,"X");
            }else if (model.getMoveAt(row,col) == Board.PLAYER_2){
                view.updateLabel(row,col,"O");
            }

        } catch (InvalidMoveException e) {
            view.showMessage("Invalid Move");
        } catch (CellIsNotEmptyException e) {
            view.showMessage("Select an empty cell");
        }
    }
}
