package tr.edu.mu.ceng.gui.tictactoe.model;

/**
 * Created by ozgur on 10/26/2017.
 */

public class InvalidMoveException extends Exception {
    public InvalidMoveException(String s) {
        super(s);
    }
}
