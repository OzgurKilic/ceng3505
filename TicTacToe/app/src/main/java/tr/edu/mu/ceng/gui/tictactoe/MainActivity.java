package tr.edu.mu.ceng.gui.tictactoe;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.Toast;

import static tr.edu.mu.ceng.gui.tictactoe.model.Board.PLAYER_1;
import static tr.edu.mu.ceng.gui.tictactoe.model.Board.PLAYER_2;

public class MainActivity extends AppCompatActivity implements BoardView {

    BoardPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        presenter = new BoardPresenter(this);
        if (savedInstanceState != null) {
            presenter.restoreBoard(savedInstanceState);
        }

        TableLayout layout = (TableLayout) findViewById(R.id.tablelayout);
        for (int row = 0; row < layout.getChildCount(); row++) {
            TableRow tableRow = (TableRow) layout.getChildAt(row);
            for (int col = 0; col < tableRow.getChildCount(); col++) {
                Button button = (Button) tableRow.getChildAt(col);
                button.setOnClickListener(new ButtonListener(row, col));
                int move = presenter.getMoveAt(row, col);
                if (move == PLAYER_1) {
                    button.setText("X");
                } else if (move == PLAYER_2) {
                    button.setText("O");
                } else {
                    button.setText("");
                }


            }
        }


    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        presenter.save(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void showMessage(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void updateLabel(int row, int col, String o) {
        TableLayout layout = (TableLayout) findViewById(R.id.tablelayout);
        TableRow tableRow = (TableRow) layout.getChildAt(row);

        Button button = (Button) tableRow.getChildAt(col);

        button.setText(o);


    }


    private class ButtonListener implements View.OnClickListener {

        int row;
        int col;

        public ButtonListener(int row, int col) {
            this.row = row;
            this.col = col;
        }

        @Override
        public void onClick(View view) {
            presenter.move(row, col);

        }
    }

}
