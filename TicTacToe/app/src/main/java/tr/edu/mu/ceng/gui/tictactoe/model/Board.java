package tr.edu.mu.ceng.gui.tictactoe.model;

import android.util.Log;

import java.io.Serializable;

/**
 * Created by ozgur on 10/26/2017.
 */

public class Board implements Serializable{
    public  int size = 3;
    public static int PLAYER_1 = 1;
    public static int PLAYER_2 = 2;

    int[][] board;
    private boolean player1 = true;

    BoardListener listener;


    public Board(int size,BoardListener listener) {
        this.size = size;
        board = new int [size][size];
        this.listener = listener;
    }


    public void move(int row, int col) throws InvalidMoveException, CellIsNotEmptyException {
        if (row >= size || col >=size){
            throw new InvalidMoveException("Select row or column should be less than the size");
        }
        if (board[row][col] != 0){
            throw new CellIsNotEmptyException();
        }

        if (player1) {
            board[row][col] = PLAYER_1;
        }else {
            board[row][col] = PLAYER_2;
        }

        checkBoard(row,col);
        player1 = !player1;
    }

    private void checkBoard(int lastRow, int lastCol) {
        boolean win = true;
        for (int row = 0; row< 3; row++){
            if (board[row][lastCol] == 0){
                win = false;

            }else if (board[row][lastCol] !=  board[lastRow][lastCol]){
                win = false;

            }
        }
        if (win){
            listener.setState(board[lastRow][lastCol] == PLAYER_1
            ? BoardListener.PLAYER_1_WIN :
                    BoardListener.PLAYER_2_WIN);
        }

        win = true;
        for (int col = 0; col< 3; col++){
            if (board[lastRow][col] == 0){
                win = false;

            }else if (board[lastRow][col] !=  board[lastRow][lastCol]){
                win = false;

            }
        }
        if (win){
            listener.setState(board[lastRow][lastCol] == PLAYER_1
                    ? BoardListener.PLAYER_1_WIN :
                    BoardListener.PLAYER_2_WIN);
        }
    }

    public int getMoveAt(int row, int col) {
        return board[row][col];
    }
}
