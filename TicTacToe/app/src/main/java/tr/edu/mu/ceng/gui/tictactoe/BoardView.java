package tr.edu.mu.ceng.gui.tictactoe;

/**
 * Created by ozgur on 10/26/2017.
 */

interface BoardView {
    void showMessage(String s);

    void updateLabel(int row, int col, String o);
}
