package tr.edu.mu.ceng.gui.intentexamples;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    Button btnStartActivity;
    Button btnThirdActivity;
    Button btnOpenBrowser;
    Button btnTakePhoto;
    Button btnShowLocation;
    EditText txtMessage;
    ImageView img;

    public static final int ANOTHER_ACTIVITY = 0;
    public static final int THIRD_ACTIVITY = 1;
    public static final int CAPTURE_IMAGE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnStartActivity = (Button) findViewById(R.id.btnStartActivity);
        txtMessage = (EditText) findViewById(R.id.txtMessage);

        btnStartActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AnotherActivity.class);

                Bundle bundle = new Bundle();
                bundle.putCharSequence("name",txtMessage.getText());
                bundle.putCharSequence("surname",txtMessage.getText());
                bundle.putInt("age",23);

                intent.putExtras(bundle);


                startActivityForResult(intent, ANOTHER_ACTIVITY);
            }
        });

        btnThirdActivity = (Button) findViewById(R.id.btnStartThird);

        btnThirdActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ThirdActivity.class);

                Bundle bundle = new Bundle();
                bundle.putCharSequence("name",txtMessage.getText());
                bundle.putCharSequence("surname",txtMessage.getText());
                bundle.putInt("age",23);

                intent.putExtras(bundle);


                startActivityForResult(intent, THIRD_ACTIVITY);
            }
        });

        btnOpenBrowser = (Button) findViewById(R.id.btnOpenBrowser);

        btnOpenBrowser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://ceng.mu.edu.tr"));
                startActivity(intent);
            }
        });

        btnTakePhoto = (Button) findViewById(R.id.btnTakePhoto);
        btnTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (intent.resolveActivity(getPackageManager()) != null){
                    startActivityForResult(intent, CAPTURE_IMAGE);
                }
            }
        });

        img = (ImageView) findViewById(R.id.imageView);

        btnShowLocation = (Button) findViewById(R.id.btnShowLocation);
        btnShowLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("geo:37.058,28.326"));
                if (intent.resolveActivity(getPackageManager()) != null){
                    startActivity(intent);
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ANOTHER_ACTIVITY && resultCode == Activity.RESULT_OK){
            txtMessage.setText(data.getCharSequenceExtra("result"));
        }else if (requestCode == THIRD_ACTIVITY && resultCode == Activity.RESULT_OK){
            btnThirdActivity.setText(data.getCharSequenceExtra("result"));
        }else if (requestCode == CAPTURE_IMAGE && resultCode == Activity.RESULT_OK){
            Bundle bundle = data.getExtras();
            Bitmap image = (Bitmap) bundle.get("data");
            img.setImageBitmap(image);
        }
    }
}
