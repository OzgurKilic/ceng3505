package tr.edu.mu.ceng.gui.intentexamples;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class AnotherActivity extends AppCompatActivity {

    EditText txtReceived;
    Button btnSendResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_another);

        Intent intent = getIntent();

        Bundle bundle = intent.getExtras();

        int age = bundle.getInt("age",40);

        txtReceived = (EditText) findViewById(R.id.txtReceivedMessage);



        txtReceived.setText(bundle.getCharSequence("name") + " age:" + age);


        btnSendResult = (Button) findViewById(R.id.btnSendResult);

        btnSendResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("result", txtReceived.getText());
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });


    }
}
