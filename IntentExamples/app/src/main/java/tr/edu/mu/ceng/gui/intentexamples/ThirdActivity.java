package tr.edu.mu.ceng.gui.intentexamples;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ThirdActivity extends AppCompatActivity {

    EditText txtReceived;
    Button btnSendResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);
        Intent intent = getIntent();

        Bundle bundle = intent.getExtras();

        int age = bundle.getInt("age",40);

        txtReceived = (EditText) findViewById(R.id.editText);



        txtReceived.setText(bundle.getCharSequence("name"));


        btnSendResult = (Button) findViewById(R.id.button);

        btnSendResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("result", txtReceived.getText());
                setResult(Activity.RESULT_OK, intent);
            }
        });
    }}
