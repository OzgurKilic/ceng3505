package tr.edu.mu.ceng.gui.listviewexample;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by ozgur on 9/28/2017.
 */

public class AnimalAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private List<Animal> animals;

    public AnimalAdapter(Activity activity, List<Animal> animals){
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.animals = animals;

    }


    public int getCount(){
        return animals.size();
    }


    public Object getItem(int position){
        return animals.get(position);
    }

    public long getItemId(int position){
        return position;
    }

    public View getView(int position, View view, ViewGroup viewgroup){
        View rowView;
        rowView = inflater.inflate(R.layout.listview_row,null);
        TextView textView = (TextView) rowView.findViewById(R.id.textView2);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.imageView);

        Animal animal = animals.get(position);

        textView.setText(animal.getType());
        imageView.setImageResource(animal.getPicId());

        return rowView;
    }
}
