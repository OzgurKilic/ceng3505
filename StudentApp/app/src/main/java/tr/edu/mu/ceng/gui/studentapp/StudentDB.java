package tr.edu.mu.ceng.gui.studentapp;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import android.support.annotation.NonNull;

/**
 * Created by ozgur on 11/16/2017.
 */
@Database(entities = {Student.class}, version=3)
public abstract class StudentDB extends RoomDatabase {

    private static StudentDB instance;
    public abstract StudentDao stuDao();


    public static synchronized StudentDB getInstance(Context ctx){
        if (instance == null){
            instance  = (StudentDB) Room.databaseBuilder(ctx, StudentDB.class,"StudentDB").
                    addMigrations(MIGRATION_1_2, MIGRATION_2_3)
            .build();
        }
        return instance;
    }


    static final Migration MIGRATION_1_2 = new Migration(1,2) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("CREATE UNIQUE INDEX StudentDB studentIdIndex on Student(studentId)");
        }
    };

    static final Migration MIGRATION_2_3 = new Migration(2,3) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE Student ADD COLUMN department TEXT");
        }
    };
}
