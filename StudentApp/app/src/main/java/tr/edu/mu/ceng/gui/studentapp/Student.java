package tr.edu.mu.ceng.gui.studentapp;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by ozgur on 11/16/2017.
 */

@Entity(indices = {@Index(value = "studentId",unique = true)})
public class Student {

    @PrimaryKey
    public Integer studentId;

    @ColumnInfo(name = "student_name")
    public String name;

    @ColumnInfo(name = "student_surname")
    public String surname;


    public String department;


    public Student(Integer studentId, String name, String surname) {
        this.studentId = studentId;
        this.name = name;
        this.surname = surname;
    }
}
