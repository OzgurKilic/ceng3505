package tr.edu.mu.ceng.gui.studentapp;

import android.database.sqlite.SQLiteConstraintException;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Student student;

    EditText txtId;
    EditText txtName;
    EditText txtSurname;
    EditText txtDepartment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtId = findViewById(R.id.txtId);
        txtName = findViewById(R.id.txtName);
        txtSurname = findViewById(R.id.txtSurname);
        txtDepartment = findViewById(R.id.txtDepartment);
    }

    void save(View view){
        student = new Student(Integer.parseInt(txtId.getText().toString()),txtName.getText().toString(),txtSurname.getText().toString());
        student.department = txtDepartment.getText().toString();
        SaveTask saveTask = new SaveTask();
        Student[] students = new Student[1];
        students[0] = student;

        saveTask.execute(students);
    }

    void load(View view){
        LoadTask loadTask = new LoadTask();
        Integer[] ids = new Integer[1];
        ids[0] = Integer.parseInt(txtId.getText().toString());

        loadTask.execute(ids);
    }

    class SaveTask extends AsyncTask<Student,Void,String>{

        @Override
        protected String doInBackground(Student... students) {
            try {
                StudentDB.getInstance(MainActivity.this).stuDao().insertStudent(students[0]);
            }catch(SQLiteConstraintException sce){
                Log.d("ConstraintException",sce.getMessage());
                sce.printStackTrace();
                return sce.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String msg) {
            if (msg != null) {
                if (msg.contains("UNIQUE constraint failed"))
                Toast.makeText(MainActivity.this,getString(R.string.unique_constraint_message), Toast.LENGTH_SHORT).show();
            }
        }
    }

    class LoadTask extends AsyncTask<Integer,Void,Student>{

        @Override
        protected Student doInBackground(Integer... ids) {

            return StudentDB.getInstance(MainActivity.this).stuDao().retrieveStudent(ids[0]);
        }

        @Override
        protected void onPostExecute(Student student) {
            if (student != null) {
                MainActivity.this.student = student;
                txtName.setText(student.name);
                txtSurname.setText(student.surname);
                txtDepartment.setText(student.department);

            }else{
                Toast.makeText(MainActivity.this, "Student is not present", Toast.LENGTH_SHORT).show();
            }

        }
    }

}
