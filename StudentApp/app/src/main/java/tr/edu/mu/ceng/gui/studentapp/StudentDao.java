package tr.edu.mu.ceng.gui.studentapp;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

/**
 * Created by ozgur on 11/16/2017.
 */

@Dao
public interface StudentDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertStudent(Student student);

    @Query("Select * from Student where studentId = :id LIMIT 1")
    Student retrieveStudent(Integer id);
}
