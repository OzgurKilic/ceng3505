package tr.edu.mu.ceng.gui.studentapp;

/**
 * Created by ozgur on 11/20/2017.
 */

import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@RunWith(AndroidJUnit4.class)
public class MigrationTest {
    private StudentDao mUserDao;
    private StudentDB mDb;

    @Before
    public void createDb() {
        Context context = InstrumentationRegistry.getTargetContext();
        mDb = Room.inMemoryDatabaseBuilder(context, StudentDB.class).build();
        mUserDao = mDb.stuDao();
    }

    @After
    public void closeDb() throws IOException {
        mDb.close();
    }

    @Test
    public void writeUserAndReadInList() throws Exception {
        Student student = new Student(1,"Ozgur","Kilic");

        mUserDao.insertStudent(student);
        Student retrievedStudent = mUserDao.retrieveStudent(1);
        assertThat("Ozgur", equalTo(retrievedStudent.name));
    }
}
