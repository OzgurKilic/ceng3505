package tr.edu.mu.ceng.gui.internalstorage;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

public class MainActivity extends AppCompatActivity {


    //Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    ImageView img;
    Button btnTakePhoto;
    Button btnSavePhoto;
    Button btnLoadPhoto;
    Bitmap image;

    public static final int CAPTURE_IMAGE = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        img = (ImageView) findViewById(R.id.imageView);
        btnTakePhoto = (Button) findViewById(R.id.btnTakePhoto);
        btnSavePhoto = (Button) findViewById(R.id.btnSavePhoto);
        btnLoadPhoto = (Button) findViewById(R.id.btnLoadPhoto);

        btnTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(intent, CAPTURE_IMAGE);
                }
            }
        });

        btnSavePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {

                    int permission = ActivityCompat.checkSelfPermission(
                            MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    if (permission != PackageManager.PERMISSION_GRANTED) {
// We don't have permission so prompt the user
                        ActivityCompat.requestPermissions(
                                MainActivity.this,
                                PERMISSIONS_STORAGE,
                                REQUEST_EXTERNAL_STORAGE
                        );
                    }

                    String imagePath = (Environment.getExternalStoragePublicDirectory
                            (Environment.DIRECTORY_PICTURES)).toString()
                            + "/" + "myimg.jpg";
                    FileOutputStream fos = new FileOutputStream(imagePath);

                    ByteArrayOutputStream bos  = new ByteArrayOutputStream();
                    image.compress(Bitmap.CompressFormat.JPEG,0,bos);

                    fos.write(bos.toByteArray());
                    fos.flush();
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        btnLoadPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String imagePath = (Environment.getExternalStoragePublicDirectory
                        (Environment.DIRECTORY_PICTURES)).toString()
                        + "/" + "myimg.jpg";
                Log.d("imagePath",imagePath);
                Bitmap image = BitmapFactory.decodeFile(imagePath);
                img.setImageBitmap(image);
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAPTURE_IMAGE && resultCode == Activity.RESULT_OK) {
            Bundle bundle = data.getExtras();
            image = (Bitmap) bundle.get("data");

            img.setImageBitmap(image);
        }
    }
}
