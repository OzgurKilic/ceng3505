package tr.edu.mu.ceng.gui.savepreferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    CheckBox cbxRemember;
    EditText txtEmail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cbxRemember = (CheckBox) findViewById(R.id.cbxRemember);
        txtEmail = (EditText) findViewById(R.id.txtEmail);

        SharedPreferences shpref = getPreferences(Context.MODE_PRIVATE);
        String email = shpref.getString(getString(R.string.email_key),"");
        txtEmail.setText(email);
    }

    void login(View view){
        if (cbxRemember.isChecked()) {
            SharedPreferences shpref = getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = shpref.edit();
            editor.putString(getString(R.string.email_key), txtEmail.getText().toString());
            editor.commit();
        }
    }
}
