package tr.edu.mu.ceng.gui.threadexample;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    TextView txtview;

    SaveTask task = new SaveTask();

    Button btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtview = (TextView)findViewById(R.id.msg);
        btn = (Button) findViewById(R.id.btnSave);
    }

    void save (View view){
        task = new SaveTask();
        task.execute(new Integer[10]);

        btn.setEnabled(false);
    }



    private class SaveTask extends AsyncTask<Integer, Integer, String>{


        @Override
        protected void onPreExecute() {
            btn.setEnabled(false);
            txtview.setText("Saving");
        }

        @Override
        protected String doInBackground(Integer[] integers) {
            for (int i = 0; i< integers.length; i++) {
                if (isCancelled()) {
                    return "Cancelled";
                }
                try {
                    //simulating the save
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                publishProgress(i);
            }
            return "Saved";
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            txtview.setText(txtview.getText() + ".");
        }

        protected void onPostExecute(String str) {
            txtview.setText(str);
            btn.setEnabled(true);
        }

        @Override
        protected void onCancelled(String str) {
            txtview.setText(str);
            btn.setEnabled(true);
        }
    }


    public void cancel(View view){
        task.cancel(true);
    }
}
